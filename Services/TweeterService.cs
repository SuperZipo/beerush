﻿using Beerush.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Tweetinvi;

namespace Beerush.Services
{
    public class TweeterService
    {
        public static async Task<TweetPost> Tweet(String txtToTweet)
        {
            var tweet = await TweetAsync.PublishTweet(txtToTweet);

            if (tweet.IsTweetPublished)
            {
                TweetPost newTweet = new TweetPost()
                {
                    Text = tweet.Text,
                    PostUrl = tweet.Url,
                    CreationDate = tweet.TweetLocalCreationDate
                };

                return newTweet;
            }

            return null;
        }
    }
}