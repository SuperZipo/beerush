﻿using Beerush.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Beerush.Services
{
    public class ShopsService
    {
        public IList<Shop> searchShops(string name)
        {
            using (var context = new BeerushContext())
            {
                IQueryable<Shop> shops = context.Shops.Where(s => s.City.ToLower().Contains(name.ToLower()));

                return shops.ToList();
            }
        }

        public object UpdateShop(Shop shop)
        {
            using (var context = new BeerushContext())
            {
                var targetShop = context.Shops.Where(s => s.ShopID == shop.ShopID).FirstOrDefault();
                targetShop.ShopName = shop.ShopName;
                try
                {
                    context.Shops.Update(targetShop);
                    context.SaveChanges();
                }
                catch (Exception)
                {
                    return false;
                }

            }
            return true;
        }
        public bool DeleteShop(int id)
        {
            using (var context = new BeerushContext())
            {
                var targetConcert = context.Shops.Where(s => s.ShopID == id).FirstOrDefault();
                context.Shops.Remove(targetConcert);

                try
                {
                    context.SaveChanges();
                }
                catch (Exception)
                {
                    return false;
                }

                return true;
            }
        }
    }
}