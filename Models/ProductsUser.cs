﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Beerush.Models
{
    public class ProductsUser
    {
        public string UserName { get; set; }
        public int UserExpense { get; set; }
    }
}