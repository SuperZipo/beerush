﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Beerush.Models
{
    public class Shop
    {
        public int ShopID { get; set; }

        public string Description { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string ShopName { get; set; }
    }
}