﻿using Microsoft.EntityFrameworkCore;

namespace Beerush.Models
{
    public class BeerushContext : DbContext
    {
        public BeerushContext()
        {
        }

        public BeerushContext(DbContextOptions<BeerushContext> options)
            : base(options)
        {

        }

        //TODO: Add get/set for all other models

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("Filename=./BeerushDB.db");
            }
        }

        public DbSet<Sale> Sales { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Inventory> Inventory { get; set; }
        public DbSet<Shop> Shops { get; set; }
    }
}