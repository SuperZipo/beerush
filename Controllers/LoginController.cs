﻿using Beerush.Models;
using Beerush.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Beerush.Controllers
{
    public class LoginController : Controller
    {
        UserServices userService = new UserServices();

        public ActionResult AttemptLogin(LoginDetails details)
        {
            LoginResult result = userService.AttemptLogin(details);
            if (result.LoginSucceeded)
            {
                User user = userService.GetUser(details.Username);
                Session["User"] = (user.FirstName);
                Session["IsAdmin"] = (user.IsAdmin);
                Session["UserID"] = (user.UserID);


            }
            return Json(result);
        }

        public ActionResult Register(RegisterDetails details)
        {
            if (userService.Register(details))
                return Json(true);

            return Json(false);
        }

        public ActionResult CheckName(string username)
        {
            bool a = userService.DoesUserExists(username);
            return Json(a, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Logout()
        {
            HttpContext.Session.Remove("User");
            HttpContext.Session.Remove("IsAdmin");

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}