﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Beerush.Models;
using Beerush.Services;

namespace Beerush.Controllers
{
    public class HomeController : Controller
    {
        private BeerushContext bc = new BeerushContext();
        ProductsPageModel model = new ProductsPageModel();
        ShopsService shopsService = new ShopsService();
        StoreService storeService = new StoreService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Store()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Manager()
        {
            return View();
        }

        public ActionResult Products(string searchTermName, int? searchTermPrice, int categoryId = -1)
        {
            if (Session["UserID"] != null)
            {
                int userId = (int)Session["UserID"];

                model.Recommended = storeService.RecommendProducts(userId);
            }

            model.Products = storeService.SearchProducts(searchTermName ?? string.Empty, searchTermPrice, categoryId);

            if (model.Categories == null)
            {
                model.Categories = storeService.GetAllCategories();
            }

            model.TopSale = storeService.getTopSaleProduct();

            return View(model);
        }

        [HttpGet]
        public ActionResult GetProduct(int id)
        {
            var product = storeService.GetProduct(id);

            if (product == null)
            {
                return HttpNotFound();
            }

            return Json(product, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserProducts()
        {
            return View();
        }

        public ActionResult SearchProducts(string termName, int price)
        {
            return View(storeService.SearchProducts(termName, price));
        }

        public ActionResult Error()
        {
            return null;
        }

        [HttpGet]
        public ActionResult BuyProduct(int userId, int id)
        {
            return Json(storeService.BuyProduct(userId, id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateProduct(Product product)
        {
            return Json(storeService.UpdateProduct(product));
        }

        [HttpPost]
        public ActionResult AddProduct(Product product)
        {
            return Json(storeService.AddProduct(product));
        }
    }
}