﻿using Beerush.Models;
using Beerush.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Beerush.Controllers
{
    public class ManagerController : Controller
    {
        TweeterService ts = new TweeterService();

        [HttpPost]
        public async Task<ActionResult> SendTweet(string txt)
        {
            TweetPost newTweet = null;

            try
            {
                newTweet = await TweeterService.Tweet(txt);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(newTweet);
        }
    }
}