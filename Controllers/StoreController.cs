﻿using Beerush.Services;
using Beerush.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Beerush.Controllers
{
    public class StoreController : Controller
    {
        StoreService storeService = new StoreService();

        public ActionResult GetAllProducts()
        {
            return Json(storeService.GetAllProductsFromInventory());
        }

        public ActionResult SalesPerCategory()
        {
            return Json(storeService.SalesPerCategory(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAllCategories()
        {
            var Categories = storeService.GetAllCategories();
            return Json(Categories, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAllAndNullCategories()
        {
            var Categories = storeService.GetAllAndNullCategories();
            return Json(Categories, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchProducts(string term, int? price)
        {
            return View(storeService.SearchProducts(term, price));
        }

        [HttpDelete]
        public ActionResult DeleteProduct(int id)
        {
            if (storeService.DeleteProduct(id))
                return Json(id);
            else
                return Json(false);
        }

        [HttpGet]
        public ActionResult ProductsStock()
        {
            return Json(storeService.GetProductsStock());
        }

        [HttpGet]
        public ActionResult GetUserProducts(int userId)
        {
            return Json(storeService.getUserProducts(userId), JsonRequestBehavior.AllowGet);
        }
    }
}