﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Beerush.Services;
using Beerush.Models;

namespace InternetFanPage.Controllers
{
    public class ShopsController : Controller
    {
        private BeerushContext bc = new BeerushContext();
        private ShopsService shopsService = new ShopsService();
        public ActionResult Shops(string searchTermName, int? searchTermPrice)
        {
            if (searchTermName == null && searchTermPrice == null)
            {
                return View(bc.Shops.AsEnumerable());
            }
            else
            {
                var model = shopsService.searchShops(searchTermName ?? string.Empty);
                return View(model);
            }

        }

        [HttpPost]
        public ActionResult UpdateShop(Shop shop)
        {
            return Json(shopsService.UpdateShop(shop));
        }

        [HttpDelete]
        public ActionResult DeleteShop(int id)
        {
            if (shopsService.DeleteShop(id))
                return Json(id);
            else
                return Json(false);
        }
    }
}
